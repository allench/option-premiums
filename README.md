```
$ npm install
$ node scraper.js {YOUR TD AMERITRADE API KEY} {EXPIRATION DATE - yyyy-MM-dd}
```

Example command
```
$ node scraper.js abcdApiKey123 2020-07-24
```

This will generate a `premiums.csv` file that you can use to sort for the highest premium to strike ratio