const axios = require('axios').default;
const fileSystem = require('fs');

const apiKey = process.argv[2];
const expirationDate = process.argv[3];

const tickers = fileSystem.readFileSync('tickers.txt', 'utf8').split('\n');

tickers.forEach((ticker, i) => {
    setTimeout(() => {
        axios.get('https://api.tdameritrade.com/v1/marketdata/chains', {
            params: {
                apikey: apiKey,
                symbol: ticker,
                contractType: 'PUT',
                strikeCount: 10,
                strategy: 'SINGLE',
                fromDate: expirationDate,
                toDate: expirationDate
            }
        })
            .then(function (response) {
                processResults(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }, i * 500);
});

function processResults(response) {
    const symbol = response.symbol;
    const underlyingPrice = response.underlyingPrice;
    const putMap = response.putExpDateMap;

    for (const [optionDate, options] of Object.entries(putMap)) {

        let closetATMPrice;
        let closetATMOption;
        for (const [optionPrice, optionList] of Object.entries(options)) {

            //Find closet OTM option
            if (underlyingPrice - optionPrice > 0) {
                closetATMPrice = optionPrice;
                closetATMOption = optionList[0];
            }
        }

        const ratio = closetATMOption.mark / closetATMPrice * 100;
        const dataRow = symbol + " " + closetATMPrice + " " + closetATMOption.mark + " " + ratio + "%\n";
        console.log(symbol + "...");

        fileSystem.writeFileSync("premiums.csv", dataRow, { 'flag': 'a' }, (error) => {
            if (error) throw error;
        });
    }
}